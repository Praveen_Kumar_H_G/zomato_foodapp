package com.learn.food.search.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.FoodItemDto;
import com.learn.food.search.dto.VenderAdminDto;
import com.learn.food.search.entity.FoodItems;
import com.learn.food.search.entity.UserRegistration;
import com.learn.food.search.entity.VenderDetails;
import com.learn.food.search.exception.ResourceConflictExists;
import com.learn.food.search.exception.ResourceNotFound;
import com.learn.food.search.exception.UnauthorizedUser;
import com.learn.food.search.repository.FoodItemsRepository;
import com.learn.food.search.repository.UserRegistrationRepository;
import com.learn.food.search.repository.VenderAdminRepository;
import com.learn.food.search.service.impl.VenderAdminServiceImpl;

@ExtendWith(SpringExtension.class)
class VenderAdminServiceImplTest {
	@Mock
	private VenderAdminRepository venderAdminRepository;

	@Mock
	private FoodItemsRepository foodItemsRepository;

	@Mock
	private UserRegistrationRepository userRegistrationRepository;

	@InjectMocks
	private VenderAdminServiceImpl venderAdminService;

	@Test
	void testCreate_PositiveScenario() {
		VenderAdminDto venderAdminDto = new VenderAdminDto("email@example.com", "Vendor Name", "password",
				"1234567890");
		when(venderAdminRepository.findByEmail(anyString())).thenReturn(Optional.empty());

		ApiResponse response = venderAdminService.create(venderAdminDto);

		assertEquals(201L, response.getCode());
		assertEquals("Successfully Vender Register", response.getMessage());
	}

	@Test
	void testCreate_NegativeScenario() {
		VenderAdminDto venderAdminDto = new VenderAdminDto("email@example.com", "Vendor Name", "password",
				"1234567890");
		when(venderAdminRepository.findByEmail(anyString())).thenReturn(Optional.of(new VenderDetails()));

		assertThrows(ResourceConflictExists.class, () -> venderAdminService.create(venderAdminDto));
	}

	@Test
	void testLogin_Vendor_PositiveScenario() {
		String email = "vendor@example.com";
		String password = "password";
		String role = "vendor";
		VenderDetails admin = new VenderDetails();
		admin.setPassword(password);
		when(venderAdminRepository.findByEmail(email)).thenReturn(Optional.of(admin));

		ApiResponse response = venderAdminService.login(email, password, role);

		assertEquals(201L, response.getCode());
		assertEquals("Logged in successfully", response.getMessage());
		assertTrue(admin.isLoggedIn());
	}

	@Test
	void testLogin_User_PositiveScenario() {
		String email = "user@example.com";
		String password = "password";
		String role = "user";
		UserRegistration userRegistration = new UserRegistration();
		userRegistration.setPassword(password);
		when(userRegistrationRepository.findByEmail(email)).thenReturn(Optional.of(userRegistration));

		ApiResponse response = venderAdminService.login(email, password, role);

		assertEquals(200L, response.getCode());
		assertEquals("user logged in succefully..", response.getMessage());
		assertTrue(userRegistration.isLoggedIn());
	}

//	@Test
//	void testLogin_InvalidRole() {
//		String email = "email@example.com";
//		String password = "password";
//		String role = "invalid_role";
//
//		assertThrows(ResourceNotFound.class, () -> venderAdminService.login(email, password, role));
//	}

//	@Test
//	void testAddFood_PositiveScenario() {
//		long venderId = 1;
//		VenderDetails admin = new VenderDetails();
//		admin.setLoggedIn(true);
//		when(venderAdminRepository.findById(venderId)).thenReturn(Optional.of(admin));
//		when(foodItemsRepository.findByFoodNameIgnoreCase(anyString())).thenReturn(Optional.empty());
//
//		FoodItemDto foodItemDto = new FoodItemDto("Pizza", 10.99);
//		ApiResponse response = venderAdminService.addFood(venderId, foodItemDto);
//
//		assertEquals(201L, response.getCode());
//		assertEquals("Food items add successfully", response.getMessage());
//	}

//	@Test
//	void testAddFood_FoodAlreadyExists() {
//		long venderId = 1;
//		VenderDetails admin = new VenderDetails();
//		admin.setLoggedIn(true);
//		when(venderAdminRepository.findById(venderId)).thenReturn(Optional.of(admin));
//		when(foodItemsRepository.findByFoodNameIgnoreCase(anyString())).thenReturn(Optional.of(new FoodItems()));
//
//		FoodItemDto foodItemDto = new FoodItemDto("Pizza", 10.99);
//		assertThrows(ResourceConflictExists.class, () -> venderAdminService.addFood(venderId, foodItemDto));
//	}

//	@Test
//	void testUpdateFood_PositiveScenario() {
//		String email = "email@example.com";
//		Long foodItemId = 1L;
//		FoodItemDto foodItemDto = new FoodItemDto("Updated Pizza", 12.99);
//		VenderDetails admin = new VenderDetails();
//		admin.setLoggedIn(true);
//		when(venderAdminRepository.findByEmail(email)).thenReturn(Optional.of(admin));
//		when(foodItemsRepository.findById(foodItemId)).thenReturn(Optional.of(new FoodItems()));
//
//		ApiResponse response = venderAdminService.updateFood(email, foodItemId, foodItemDto);
//
//		assertEquals(200L, response.getCode());
//		assertEquals("Food item updated successfully", response.getMessage());
//	}

//	@Test
//	void testUpdateFood_FoodItemNotFound() {
//		String email = "email@example.com";
//		Long foodItemId = 1L;
//		FoodItemDto foodItemDto = new FoodItemDto("Updated Pizza", 12.99);
//		VenderDetails admin = new VenderDetails();
//		admin.setLoggedIn(true);
//		when(venderAdminRepository.findByEmail(email)).thenReturn(Optional.of(admin));
//		when(foodItemsRepository.findById(foodItemId)).thenReturn(Optional.empty());
//
//		assertThrows(ResourceNotFound.class,
//				() -> venderAdminService.updateFood(email, foodItemId, foodItemDto));
//	}



//	@Test
//	void testDeleteFood_UnauthorizedUser() {
//		String email = "email@example.com";
//		Long foodItemId = 1L;
//		VenderAdminRegister admin = new VenderAdminRegister();
//		admin.setLoggedIn(false);
//		when(venderAdminRepository.findByEmail(email)).thenReturn(Optional.of(admin));
//
//		assertThrows(UnauthorizedUser.class, () -> venderAdminService.deleteFood(foodItemId));
//	}

}
