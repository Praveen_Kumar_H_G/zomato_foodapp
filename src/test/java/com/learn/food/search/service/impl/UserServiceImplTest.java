package com.learn.food.search.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.CartItemDto;
import com.learn.food.search.dto.UserDto;
import com.learn.food.search.entity.CartItem;
import com.learn.food.search.entity.FoodItems;
import com.learn.food.search.entity.UserRegistration;
import com.learn.food.search.entity.VenderDetails;
import com.learn.food.search.exception.ResourceConflictExists;
import com.learn.food.search.repository.CartItemRepository;
import com.learn.food.search.repository.FoodItemsRepository;
import com.learn.food.search.repository.UserRegistrationRepository;
import com.learn.food.search.repository.VenderAdminRepository;
import com.learn.food.search.service.impl.UserServiceImpl;

@ExtendWith(SpringExtension.class)
 class UserServiceImplTest {
	
	@InjectMocks
	private UserServiceImpl userServiceImpl;
	@Mock
	private  UserRegistrationRepository userRegistrationRepository;
	@Mock
	private  FoodItemsRepository foodItemsRepository;
	@Mock
	private  VenderAdminRepository venderAdminRepository;
	@Mock
	private  CartItemRepository cartItemRepository;
	
	@Test
	void userRegisteredAlreadyExist() {
		UserRegistration user = new UserRegistration();
		user.setEmail("Praveen@gmail.com");
 
		Mockito.when(userRegistrationRepository.findByEmail("Praveen@gmail.com")).thenReturn(Optional.of(user));
		UserDto userDto = new UserDto();
		userDto.setUserEmail("Praveen@gmail.com");
		assertThrows(ResourceConflictExists.class, () -> userServiceImpl.userRegister(userDto));
	}
	
	@Test
	void userRegistraionSuccessfull() {
		UserRegistration user = new UserRegistration();
		user.setEmail("Praveen@gmail.com");
		Mockito.when(userRegistrationRepository.findByEmail("Praveen@gmail.com")).thenReturn(Optional.empty());
		Mockito.when(userRegistrationRepository.save(user)).thenReturn(user);
		UserDto userDto = new UserDto();
		userDto.setUserEmail("Praveen@gmail.com");
		ApiResponse register = userServiceImpl.userRegister(userDto);
		assertEquals("succefuuly register..", register.getMessage());
		

	}
//	@Test
//    public void testAddToCartSuccess() {
//        // Mock data
//        String email = "test@example.com";
//        CartItemDto cartItemDto = new CartItemDto();
//        cartItemDto.setQuantity(1);
//        cartItemDto.setFoodName("Pizza");
//        cartItemDto.setVendorName("PizzaVendor");
// 
//        UserRegistration registration = new UserRegistration();
//        registration.setEmail(email);
//        registration.setLoggedIn(true);
// 
//        FoodItems foodItem = new FoodItems();
//        foodItem.setFoodName("Pizza");
// 
//        VenderDetails vendor = new VenderDetails();
//        vendor.setName("PizzaVendor");
// 
//        // Mock repository behaviors
//        when(userRegistrationRepository.findByEmail(email)).thenReturn(Optional.of(registration));
//        when(foodItemsRepository.findByFoodNameIgnoreCase("Pizza")).thenReturn(Optional.of(foodItem));
//        when(venderAdminRepository.findByNameIgnoreCase("PizzaVendor")).thenReturn(Optional.of(vendor));
// 
//        // Execute the method
//        ApiResponse response = userServiceImpl.addToCart(email, cartItemDto);
// 
//        // Verify the result
//        assertEquals(201L, response.getCode());
//        assertEquals("food add cart successfully..", response.getMessage());
//        verify(cartItemRepository, times(1)).save(any(CartItem.class));
//    }
}
