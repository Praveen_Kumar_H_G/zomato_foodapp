package com.learn.food.search.dto;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDto {
	@Valid
	private List<CartItemDto>orderItemDtos;
	@NotNull(message = "enter the user Id")
	private long userId;

}
