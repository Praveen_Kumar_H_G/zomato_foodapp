package com.learn.food.search.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FoodItemDto {
	
	@NotBlank(message = "food name is requred")
	private String foodName;
	@Min(10)
	@NotNull(message = "food price is requred")
	private double price;
	private String foodType;
	private String foodItemsAddBy;
	
	

}
