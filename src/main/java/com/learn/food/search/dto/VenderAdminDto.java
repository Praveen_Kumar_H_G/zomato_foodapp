package com.learn.food.search.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VenderAdminDto {
	@NotBlank(message = "firstName required")
	private String vendeName;
	@Email(message = "invalid email")
	@NotBlank
	private String venderEmail;
	@NotBlank(message = "specify password")
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$@!%&*?])[A-Za-z\\d#$@!%&*?]{8,}$", message = "Password must contain atleast 1 uppercase letter, 1 lowercase letter,  1 special character, 1 number, Min 8 characters.")
	private String password;
	@NotBlank(message = "phone number is required")
	@Pattern(regexp = "[6-9][\\d]{9}", message = "invalid phone number")
	private String venderPhone;

}
