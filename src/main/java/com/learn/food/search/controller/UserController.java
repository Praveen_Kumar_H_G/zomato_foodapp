package com.learn.food.search.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.CartItemDto;
import com.learn.food.search.dto.UserDto;
import com.learn.food.search.service.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Praveenakumara H G
 * 
 */
@RestController
@RequestMapping("api/")
@RequiredArgsConstructor
@Slf4j
public class UserController {

	private final UserService userService;

	/**
	 * This method handles the HTTP POST request to register a new user. It takes
	 * the user details from the request body in the form of a UserDto and performs
	 * the registration process.
	 *
	 * @param userDto The details of the user to be registered, including username,
	 *                email, password, etc.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         user registration operation. Custom status code and message upon successful
	 *         operation.
	 */
	@PostMapping("/user")
	public ResponseEntity<ApiResponse> userRegister(@Valid @RequestBody UserDto userDto) {
		log.info("Received request to register user with email: {}", userDto.getUserEmail());
		return new ResponseEntity<>(userService.userRegister(userDto), HttpStatus.CREATED);
	}

	/**
	 * This method handles the HTTP POST request to add a food item to a user's
	 * shopping cart. It takes the user's email, food ID, and the CartItemDto object
	 * from the request body to add the item to the user's cart.
	 *
	 * @param email       The email of the user to whose cart the item is being
	 *                    added.
	 * @param foodId      The ID of the food item to be added to the cart.
	 * @param cartItemDto The details of the item being added to the cart, including
	 *                    quantity, etc.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         add to cart operation. Custom status code and message upon successful
	 *         operation.
	 */
	@PostMapping("cart/{email}")
	public ResponseEntity<ApiResponse> aaddToCart(@PathVariable String email, @PathVariable long foodId,
			@Valid @RequestBody CartItemDto cartItemDto) {
		log.info("Successfully added food item with ID: {} to the cart of user with email: {}", foodId, email);
		return new ResponseEntity<>(userService.addToCart(email, foodId, cartItemDto), HttpStatus.CREATED);
	}

	/**
	 * This method handles the HTTP POST request to add an order for a user. It
	 * takes the cart item ID and the user's email as parameters to process the
	 * order.
	 * 
	 * @param cartItemId The ID of the cart item being ordered.
	 * @param email      The email of the user placing the order.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         order the food operation. Custom status code and message upon successful
	 *         operation.
	 */

	@PostMapping("/{cartid}")
	public ResponseEntity<ApiResponse> addOrder(@PathVariable long cartItemId, @RequestParam String email) {
		log.info("Received request to add order for cartItemId: {} and email: {}", cartItemId, email);
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.addOrders(cartItemId, email));
	}

}
