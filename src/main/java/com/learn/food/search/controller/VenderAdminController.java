package com.learn.food.search.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.FoodItemDto;
import com.learn.food.search.dto.VenderAdminDto;
import com.learn.food.search.service.VenderAdminService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


/**
 * @author Praveenakumara H G
 */

@RestController
@RequestMapping("")
@RequiredArgsConstructor
@Slf4j
public class VenderAdminController {

	private final VenderAdminService venderAdminService;

	/**
	 * This method handles the HTTP POST request to register a new vendor (vender).
	 * It takes the vendor details from the request body in the form of a
	 * VenderAdminDto and performs the registration process for the vendor.
	 *
	 * @param venderAdminDto The details of the vendor to be registered, including
	 *                       name, contact info, etc.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         registration operation. custom status code and message upon
	 *         successful operation.
	 */
	@PostMapping("/register")
	public ResponseEntity<ApiResponse> addVender(@Valid @RequestBody VenderAdminDto venderAdminDto) {
		log.info("Successfully registered vendor with name: {}", venderAdminDto.getVendeName());

		return new ResponseEntity<>(venderAdminService.create(venderAdminDto), HttpStatus.CREATED);
	}

	/**
	 * This method handles the HTTP POST request to authenticate a vendor (vender)
	 * login. It takes the vendor's email, password, and role from the request
	 * parameters and verifies the login credentials for the vendor.
	 *
	 * @param email    The email of the vendor trying to log in.
	 * @param password The password provided by the vendor for authentication.
	 * @param role     The role of the vendor or user (used to differentiate between
	 *                 various roles).
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         login operation. The custom status code and message upon successful
	 *         operation.
	 */
	@PostMapping("/vender/login")
	public ResponseEntity<ApiResponse> login(@Valid @NotBlank @RequestParam String email, @RequestParam String password,
			@RequestParam String role) {
		log.info("Successfully registered vendor with name: {}");
		return new ResponseEntity<>(venderAdminService.login(email, password, role), HttpStatus.OK);
	}

	/**
	 * This method handles the HTTP POST request to add a new food item to the
	 * vendor's inventory. It takes the vendor ID and food item details from the
	 * request body in the form of a FoodItemDto and adds the food item to the
	 * inventory of the specified vendor.
	 *
	 * @param id          The ID of the vendor to which the food item is being
	 *                    added.
	 * @param foodItemDto The details of the food item being added, including name,
	 *                    description, price, etc.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         add food operation. The custom status code and message upon
	 *         successful operation.
	 */
	@PostMapping("/food/add/{id}")
	public ResponseEntity<ApiResponse> addFood(@PathVariable long id, @Valid @RequestBody FoodItemDto foodItemDto) {
		log.info("Successfully added food item: {} for vendor with ID: {}", foodItemDto.getFoodName(), id);
		return new ResponseEntity<>(venderAdminService.addFood(id, foodItemDto), HttpStatus.CREATED);
	}

	/**
	 * This method handles the HTTP PUT request to update an existing food item. It
	 * takes the email of the vendor, the food item ID, and the updated details of
	 * the food item in the form of a FoodItemDto from the request body.
	 * 
	 * @param email       The email of the vendor requesting the food item update.
	 * @param foodItemId  The ID of the food item to be updated.
	 * @param foodItemDto The updated details of the food item, including name,
	 *                    description, price, etc.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         update operation. The custom status code and message upon successful
	 *         update of the food item.
	 */
	@PutMapping("/{foodItemId}")
	public ResponseEntity<ApiResponse> updateFoodItem(@RequestHeader("email") String email,
			@PathVariable("foodItemId") Long foodItemId, @RequestBody FoodItemDto foodItemDto) {
		log.info("Successfully updated food item with ID: {} for vendor with email: {}", foodItemId, email);
		return ResponseEntity.status(HttpStatus.OK).body(venderAdminService.updateFood(email, foodItemId, foodItemDto));
	}

	/**
	 * This method handles the HTTP DELETE request to remove a food item from the
	 * vendor's inventory. It takes the food item ID as a path variable and deletes
	 * the specified food item.
	 *
	 * @param foodItemId The ID of the food item to be deleted from the inventory.
	 * @return A ResponseEntity containing the ApiResponse with the result of the
	 *         delete operation. The custom status code and message upon successful
	 *         deletion of the food item.
	 */
	@DeleteMapping("/food/{foodItemId}")
	public ResponseEntity<ApiResponse> deleteFood(@PathVariable long foodItemId) {
		log.info("Successfully deleted food item with ID: {}", foodItemId);
		return ResponseEntity.status(HttpStatus.OK).body(venderAdminService.deleteFood(foodItemId));
	}

}
