package com.learn.food.search.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.food.search.entity.Orders;

public interface OrderRepository extends JpaRepository<Orders, Long>{

}
