package com.learn.food.search.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.food.search.entity.UserRegistration;


public interface UserRegistrationRepository extends JpaRepository<UserRegistration, Long> {

	Optional<UserRegistration>  findByEmail(String email);
}
