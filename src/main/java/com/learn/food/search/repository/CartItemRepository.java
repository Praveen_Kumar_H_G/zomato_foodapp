package com.learn.food.search.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.food.search.entity.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {

}
