package com.learn.food.search.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.food.search.entity.FoodItems;

public interface FoodItemsRepository extends JpaRepository<FoodItems, Long> {

	Optional<FoodItems> findByFoodNameIgnoreCase(String foodName);

//	List<FoodItems> findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase(String foodName, String foodType);
//
//
//
//	List<FoodItems> findByFoodNameContainingIgnoreCase(String foodName);
//
//	List<FoodItems> findByFoodTypeContainingIgnoreCase(String foodType);



}
