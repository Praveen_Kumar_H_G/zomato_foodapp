package com.learn.food.search.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.food.search.entity.VenderDetails;



public interface VenderAdminRepository extends JpaRepository<VenderDetails, Long> {
	
	Optional<VenderDetails> findByEmail(String email);
	
	Optional<VenderDetails> findByNameIgnoreCase(String name);

}
