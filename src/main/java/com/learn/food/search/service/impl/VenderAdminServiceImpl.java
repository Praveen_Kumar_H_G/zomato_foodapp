package com.learn.food.search.service.impl;

import java.util.Optional;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.FoodItemDto;
import com.learn.food.search.dto.VenderAdminDto;
import com.learn.food.search.entity.FoodItems;
import com.learn.food.search.entity.UserRegistration;
import com.learn.food.search.entity.VenderDetails;
import com.learn.food.search.exception.ResourceConflictExists;
import com.learn.food.search.exception.ResourceNotFound;
import com.learn.food.search.exception.UnauthorizedUser;
import com.learn.food.search.repository.FoodItemsRepository;
import com.learn.food.search.repository.UserRegistrationRepository;
import com.learn.food.search.repository.VenderAdminRepository;
import com.learn.food.search.service.VenderAdminService;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class VenderAdminServiceImpl implements VenderAdminService {

	private final VenderAdminRepository venderAdminRepository;
	private final FoodItemsRepository foodItemsRepository;
	private final UserRegistrationRepository userRegistrationRepository;

	@Override
	public ApiResponse create(VenderAdminDto venderAdminDto) {
		Optional<VenderDetails> admin = venderAdminRepository.findByEmail(venderAdminDto.getVenderEmail());
		if (admin.isPresent()) {
			throw new ResourceConflictExists("Vender admin already register");
		}
		VenderDetails admin1 = VenderDetails.builder().email(venderAdminDto.getVenderEmail())
				.name(venderAdminDto.getVendeName()).password(venderAdminDto.getPassword())
				.phone(venderAdminDto.getVenderPhone()).build();
		venderAdminRepository.save(admin1);

		return ApiResponse.builder().code(201l).message("Successfully Vender Register").build();
	}

	@Override
	public ApiResponse login(String email, String password, String role) {
		if (role.equalsIgnoreCase("vendor")) {

			VenderDetails admin = venderAdminRepository.findByEmail(email)
					.orElseThrow(() -> new ResourceNotFoundException("Invalid Vender email id "));

			if (admin.getPassword().equals(password)) {
				admin.setLoggedIn(true);
				venderAdminRepository.save(admin);
				return ApiResponse.builder().code(201l).message("Logged in successfully").build();
			} else
				throw new UnauthorizedUser("Please Enter valid Password");
		} else if (role.equalsIgnoreCase("user")) {
			UserRegistration userRegistration = userRegistrationRepository.findByEmail(email)
					.orElseThrow(() -> new ResourceNotFoundException("invalid user email id"));
			if (userRegistration.getPassword().equals(password)) {
				userRegistration.setLoggedIn(true);
				userRegistrationRepository.save(userRegistration);
				return ApiResponse.builder().code(200l).message("user logged in succefully..").build();
			} else
				throw new UnauthorizedUser("please enter valid password");

		} else
			throw new ResourceNotFoundException("invalid role");

	}

	@Override
	public ApiResponse addFood(long id, FoodItemDto foodItemDto) {
		VenderDetails adminRegister = venderAdminRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFound("Vender email Id not exits"));

		if (adminRegister.isLoggedIn()) {
			Optional<FoodItems> byFoodNameIgnoreCase = foodItemsRepository
					.findByFoodNameIgnoreCase(foodItemDto.getFoodName());
			if (byFoodNameIgnoreCase.isEmpty()) {

				FoodItems fItems = FoodItems.builder().foodName(foodItemDto.getFoodName())
						.foodPrice(foodItemDto.getPrice()).foodItemsAddBy(foodItemDto.getFoodItemsAddBy())
						.vendor(adminRegister).build();

				foodItemsRepository.save(fItems);
				return ApiResponse.builder().code(201l).message("Food items add successfully").build();
			} else
				throw new ResourceConflictExists("Food already present");
		} else
			throw new UnauthorizedUser("Vender Login to to add the food");

	}

	@Override
	public ApiResponse updateFood(String email, Long foodItemId, FoodItemDto foodItemDto) {
		Optional<VenderDetails> byEmail = venderAdminRepository.findByEmail(email);
		if (byEmail.isEmpty()) {
			throw new ResourceNotFound("Vender email Id does not exist");
		}
		VenderDetails admin = byEmail.get();
		if (admin.isLoggedIn()) {
			Optional<FoodItems> optionalFoodItem = foodItemsRepository.findById(foodItemId);
			if (optionalFoodItem.isPresent()) {
				FoodItems foodItem = optionalFoodItem.get();
				foodItem.setFoodName(foodItemDto.getFoodName());
				foodItem.setFoodPrice(foodItemDto.getPrice());
				foodItemsRepository.save(foodItem);
				return ApiResponse.builder().code(200l).message("Food item updated successfully").build();
			} else {
				throw new ResourceNotFound("Food item not found");
			}
		} else {
			throw new UnauthorizedUser("Vender must be logged in to update food items");
		}
	}

	@Override
	public ApiResponse deleteFood(long foodItemId) {
		FoodItems foodItems = foodItemsRepository.findById(foodItemId)
				.orElseThrow(() -> new ResourceNotFound("Foood not found"));
		VenderDetails vendor = foodItems.getVendor();
		if (vendor == null || !vendor.isLoggedIn()) {
			throw new ResourceConflictExists("Vendor should be logged in");
		}
		foodItemsRepository.deleteById(foodItemId);
		return ApiResponse.builder().code(200l).message("Food deleted successfully").build();
	}

}
