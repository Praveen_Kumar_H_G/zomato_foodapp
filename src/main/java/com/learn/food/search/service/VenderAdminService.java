package com.learn.food.search.service;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.FoodItemDto;
import com.learn.food.search.dto.VenderAdminDto;

public interface VenderAdminService {

	ApiResponse create( VenderAdminDto venderAdminDto);

	ApiResponse login(String email, String password,String role);

	ApiResponse addFood(long id, FoodItemDto foodItemDto);

	ApiResponse updateFood(String email, Long foodItemId, FoodItemDto foodItemDto);

	ApiResponse deleteFood(long foodItemId);




}
