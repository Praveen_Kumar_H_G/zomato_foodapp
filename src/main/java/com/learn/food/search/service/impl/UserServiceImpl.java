package com.learn.food.search.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.CartItemDto;
import com.learn.food.search.dto.UserDto;
import com.learn.food.search.entity.CartItem;
import com.learn.food.search.entity.FoodItems;
import com.learn.food.search.entity.UserRegistration;
import com.learn.food.search.entity.VenderDetails;
import com.learn.food.search.exception.ResourceConflictExists;
import com.learn.food.search.exception.ResourceNotFound;
import com.learn.food.search.exception.UnauthorizedUser;
import com.learn.food.search.repository.CartItemRepository;
import com.learn.food.search.repository.FoodItemsRepository;
import com.learn.food.search.repository.UserRegistrationRepository;
import com.learn.food.search.repository.VenderAdminRepository;
import com.learn.food.search.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRegistrationRepository userRegistrationRepository;
	private final FoodItemsRepository foodItemsRepository;
	private final VenderAdminRepository venderAdminRepository;
	private final CartItemRepository cartItemRepository;


	@Override
	public ApiResponse userRegister(UserDto userDto) {
		Optional<UserRegistration> byEmail = userRegistrationRepository.findByEmail(userDto.getUserEmail());
		if (byEmail.isPresent()) {
			throw new ResourceConflictExists("User email Id already register..");
		}

		UserRegistration user = UserRegistration.builder().email(userDto.getUserEmail()).name(userDto.getUserName())
				.password(userDto.getPassword()).phone(userDto.getUserPhone()).build();

		userRegistrationRepository.save(user);
		return ApiResponse.builder().code(201l).message("succefuuly register..").build();
	}

	@Override
	public ApiResponse addToCart(String email, long foodId, CartItemDto cartItemDto) {
		UserRegistration registration = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceConflictExists("User email id is not exits.."));
		FoodItems items = foodItemsRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFound("Invalid Food ID"));

		if (registration.isLoggedIn()) {

			FoodItems foodName = foodItemsRepository.findByFoodNameIgnoreCase(cartItemDto.getFoodName())
					.orElseThrow(() -> new ResourceNotFound("food items not exits"));
			String food = foodName.getFoodName();

			VenderDetails vendorName = venderAdminRepository.findByNameIgnoreCase(cartItemDto.getVendorName())
					.orElseThrow(() -> new ResourceNotFound("Vender name not exits"));
			String vendeName = vendorName.getName();
			double price = foodName.getFoodPrice() * cartItemDto.getQuantity();

			CartItem cartItem = CartItem.builder().foodName(food).vendorName(vendeName).totalPrice(price)
					.quantity(cartItemDto.getQuantity()).foodItems(items).paymentMethod(cartItemDto.getPaymentMethod())
					.PaymentStatus("pending").build();

			cartItemRepository.save(cartItem);
			return ApiResponse.builder().code(201l).message("food add cart successfully..").build();
		} else
			throw new UnauthorizedUser("User must logged in to add the cart");
	}


	@Override
	public ApiResponse addOrders(long cartItemId, String email) {

		UserRegistration registration = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFound("User email id is not exits.."));
		CartItem cartItem = cartItemRepository.findById(cartItemId)
				.orElseThrow(() -> new ResourceNotFound("Invalid CartID"));
		if (registration.isLoggedIn()) {
			cartItem.setPaymentStatus("Done");
			cartItemRepository.save(cartItem);
			return ApiResponse.builder().code(200l).message("Payment Succefully done..").build();

		} else
			throw new UnauthorizedUser("Customer Should be login to Conform the payment");
	}

}
