package com.learn.food.search.service;

import java.util.List;

import com.learn.food.search.dto.ApiResponse;
import com.learn.food.search.dto.CartItemDto;
import com.learn.food.search.dto.UserDto;
import com.learn.food.search.entity.FoodItems;

public interface UserService {

	ApiResponse userRegister(UserDto userDto);

	ApiResponse addToCart(String email,long foodId, CartItemDto cartItemDto);

//	List<FoodItems> searchFoodItems(String foodName, String foodType);

	ApiResponse addOrders(long cartItemId,String email);

	


}
