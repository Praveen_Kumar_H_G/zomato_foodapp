package com.learn.food.search.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FoodItems {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long foodId;
	private String foodName;
	@ManyToOne(cascade = CascadeType.ALL)
	private VenderDetails vendor;
	private double foodPrice;
	private String foodType;
	private String foodItemsAddBy;
	@CreationTimestamp
	private LocalDateTime foodItemPostedOn;
}
